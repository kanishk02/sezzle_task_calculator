"""
Python3,
Run-command: 'python3 calculator.py'
"""

"""
Taking list expression as an input and checking various standard level validations on it.
Producing an appropriate expression to be evaluated.
"""
def get_valid_and_apt_exp(list_exp):
    current_opd = ''
    operators_list = ['/', '*', '+', '-']
    apt_list_exp = []
    sign_operators = ['+', '-']
    if list_exp[0] in operators_list and list_exp[0] not in sign_operators:
        return 'Error! The first character of the expression should be a signed/unsigned numeric value.'
    if list_exp[-1] in operators_list:
        return 'Error! The last character of the expression should be a numeric value.'
    if '/' in list_exp and list_exp[(list_exp.index('/')+1)] == '0':
        return 'Error! Expression evaluation is undefined.'
    for index, item in enumerate(list_exp):
        if item in operators_list and list_exp[index]==list_exp[index+1]:
            return 'Error! No consecutive operators allowed, there should be a numeric value between every pair of operators.'
        if item not in operators_list:
            current_opd = current_opd+item
        else:
            if current_opd != '':
                apt_list_exp = apt_list_exp+[current_opd,item]
            else:
                apt_list_exp.append(item)
            current_opd = ''
        if index == len(list_exp)-1:
            apt_list_exp.append(current_opd)

    if apt_list_exp[0] == '+' or apt_list_exp[0] == '-':
        apt_list_exp[1] = apt_list_exp[0]+apt_list_exp[1]
        apt_list_exp = apt_list_exp[1:]

    return apt_list_exp

"""
Evaluating the finalised appropriate expression based on BODMAS rule.
"""
def evaluate(list_exp):
    if '/' in list_exp and '*' in list_exp:
        if list_exp.index('/')<list_exp.index('*'):
            list_exp = divide(list_exp)
        else:
            list_exp = multiply(list_exp)
        return(evaluate(list_exp))
    if '/' in list_exp:
        list_exp = divide(list_exp)
        return(evaluate(list_exp))
    elif '*' in list_exp:
        list_exp = multiply(list_exp)
        return(evaluate(list_exp))
    if '+' in list_exp and '-' in list_exp:
        if list_exp.index('+')<list_exp.index('-'):
            list_exp = sum(list_exp)
        else:
            list_exp = subtract(list_exp)
        return(evaluate(list_exp))
    if '+' in list_exp:
        list_exp = sum(list_exp)
        return(evaluate(list_exp))
    elif '-' in list_exp:
        list_exp = subtract(list_exp)
        return(evaluate(list_exp))
    if len(list_exp[0].split(".")[1]) == 1 and list_exp[0].split(".")[1]=='0':
        return str(int(float(list_exp[0])))
    return list_exp[0]

def sum(list_exp):
    index_opr = list_exp.index('+')
    index_op1 = index_opr-1
    index_op2 = index_opr+1
    value = str(float(list_exp[index_op1])+float(list_exp[index_op2]))
    list_exp = list_exp[:index_op1]+list_exp[index_op2+1:]
    list_exp.insert(index_op1,value)
    return list_exp

def subtract(list_exp):
    index_opr = list_exp.index('-')
    index_op1 = index_opr-1
    index_op2 = index_opr+1
    value = str(float(list_exp[index_op1])-float(list_exp[index_op2]))
    list_exp = list_exp[:index_op1]+list_exp[index_op2+1:]
    list_exp.insert(index_op1,value)
    return list_exp

def multiply(list_exp):
    index_opr = list_exp.index('*')
    index_op1 = index_opr-1
    index_op2 = index_opr+1
    value = str(float(list_exp[index_op1])*float(list_exp[index_op2]))
    list_exp = list_exp[:index_op1]+list_exp[index_op2+1:]
    list_exp.insert(index_op1,value)
    return list_exp

def divide(list_exp):
    index_opr = list_exp.index('/')
    index_op1 = index_opr-1
    index_op2 = index_opr+1
    value = str(float(list_exp[index_op1])/float(list_exp[index_op2]))
    list_exp = list_exp[:index_op1]+list_exp[index_op2+1:]
    list_exp.insert(index_op1,value)
    return list_exp

"""
Converting the user input expression into a list of sequential characters to validate.
Change it to an appropriate expression to be evaluated using BODMAS rule.
"""
if __name__=="__main__":
    list_exp = list(input("Enter an expression to be evaluated, e.g. '2+3*4-6/2': "))
    output = get_valid_and_apt_exp(list_exp)
    if type(output) == str and output.find('Error') != -1:
        print(output)
    else:
        apt_list_exp = output
        print("result is: " + evaluate(apt_list_exp))